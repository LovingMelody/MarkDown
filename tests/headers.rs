extern crate markdown_parser;
use markdown_parser::{Headers, MarkDown};

fn get_header(txt: &str) -> Headers {
    match txt.parse() {
        Ok(header) => header,
        Err(why) => panic!("Failed to parse {}, {:?}", txt, why),
    }
}

#[test]
fn header_1() {
    ["# Hello World", "# Hello World #"]
        .into_iter()
        .for_each(|txt| match get_header(txt) {
            Headers::H1(text) => assert_eq!(text, "Hello World"),
            header => panic!("Expected H1 header, found {:?}", header),
        });
}

#[test]
fn header_2() {
    ["## Hello World", "## Hello World ##"]
        .into_iter()
        .for_each(|txt| match get_header(txt) {
            Headers::H2(text) => assert_eq!(text, "Hello World"),
            header => panic!("Expected H2 header, found {:?}", header),
        });
}

#[test]
fn header_3() {
    ["### Hello World", "### Hello World ###"]
        .into_iter()
        .for_each(|txt| match get_header(txt) {
            Headers::H3(text) => assert_eq!(text, "Hello World"),
            header => panic!("Expected H3 header, found {:?}", header),
        });
}

#[test]
fn header_4() {
    ["#### Hello World", "#### Hello World ####"]
        .into_iter()
        .for_each(|txt| match get_header(txt) {
            Headers::H4(text) => assert_eq!(text, "Hello World"),
            header => panic!("Expected H4 header, found {:?}", header),
        });
}

#[test]
fn header_5() {
    ["##### Hello World", "##### Hello World ######"]
        .into_iter()
        .for_each(|txt| match get_header(txt) {
            Headers::H5(text) => assert_eq!(text, "Hello World"),
            header => panic!("Expected H5 header, found {:?}", header),
        });
}

#[test]
fn header_6() {
    ["###### Hello World", "###### Hello World ######"]
        .into_iter()
        .for_each(|txt| match get_header(txt) {
            Headers::H6(text) => assert_eq!(text, "Hello World"),
            header => panic!("Expected H6 header, found {:?}", header),
        });
}

// #[test]
// fn multi_line_header() {
//     match get_header("Hello World\n===") {
//         Headers::H1(text) => assert_eq!(text, "Hello World"),
//         header => panic!("Expected H6 header, found {:?}", header)
//     }
// }

#[test]
fn multiple_headers() {
    match markdown_parser::parse("# Hello World\n## It Is\n### Very Cold\n#### Outside Today") {
        Ok(results) => results
            .into_iter()
            .map(|r| match r {
                MarkDown::Header(head) => head,
                err => panic!("Expected Header, found {:?}", err),
            })
            .enumerate()
            .for_each(|(i, e)| match i {
                0 => match e {
                    Headers::H1(txt) => assert_eq!(txt, "Hello World"),
                    err => panic!("Expected H1(\"Hello World\"), found {:?}", err),
                },
                1 => match e {
                    Headers::H2(txt) => assert_eq!(txt, "It Is"),
                    err => panic!("Expected H2(\"It Is\"), found {:?}", err),
                },
                2 => match e {
                    Headers::H3(txt) => assert_eq!(txt, "Very Cold"),
                    err => panic!("Expected H3(\"Very Cold\"), found {:?}", err),
                },
                3 => match e {
                    Headers::H4(txt) => assert_eq!(txt, "Outside Today"),
                    err => panic!("Expected H4(\"Outside Today\"), found {:?}", err),
                },
                _ => unreachable!(),
            }),
        Err(why) => panic!("{:?}", why),
    }
}
