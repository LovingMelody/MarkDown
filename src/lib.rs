use std::str::FromStr;

// TODO:

#[derive(Debug)]
pub enum Headers {
    H1(String),
    H2(String),
    H3(String),
    H4(String),
    H5(String),
    H6(String),
}

impl FromStr for Headers {
    type Err = MarkDownParserError;
    fn from_str(s: &str) -> Result<Headers, MarkDownParserError> {
        if !s.starts_with('#') | s.find('\n').is_some() {
            return Err(MarkDownParserError::FailedToParseHeader(s.to_string()));
        }
        let (text, trimmed_start) = trim_hashtags(s);
        if trimmed_start > 0 {
            Ok(match trimmed_start {
                0 => unreachable!(),
                1 => Headers::H1(text.to_string()),
                2 => Headers::H2(text.to_string()),
                3 => Headers::H3(text.to_string()),
                4 => Headers::H4(text.to_string()),
                5 => Headers::H5(text.to_string()),
                _ => Headers::H6(text.to_string()),
            })
        } else {
            Err(MarkDownParserError::FailedToParseHeader(s.to_string()))
        }
    }
}

#[derive(Debug)]
pub enum LinkType {
    Absolute(String),
    NamedAbsolute(String, String),
    NamedRelative(String, String),
}

impl FromStr for LinkType {
    type Err = MarkDownParserError;
    fn from_str(s: &str) -> Result<LinkType, MarkDownParserError> {
        if s.find('\n').is_some() {
            Err(MarkDownParserError::FailedToParseLink(s.to_string()))
        } else if s.starts_with('[') & s.ends_with(')') {
            if let Some(open_pos) = s.find('(') {
                // ("[TEXT]", "(URL)")
                let (unparsed_text, unparsed_url) = s.split_at(open_pos);
                // "TEXT"
                let parsed_text = unparsed_text
                    .trim_left_matches('[')
                    .trim_right_matches(']')
                    .to_string();
                // "URL"
                let parsed_url = unparsed_url
                    .trim_left_matches('(')
                    .trim_right_matches(')')
                    .trim()
                    .to_string();
                if parsed_url.split_whitespace().collect::<Vec<&str>>().len() != 1 {
                    Err(MarkDownParserError::FailedToParseLink(s.to_string()))
                } else if parsed_url.find("://").is_some() {
                    Ok(LinkType::NamedAbsolute(parsed_text, parsed_url))
                } else {
                    Ok(LinkType::NamedRelative(parsed_text, parsed_url))
                }
            } else {
                Err(MarkDownParserError::FailedToParseLink(s.to_string()))
            }
        } else if s.find("://").is_some() & (s.split_whitespace().collect::<Vec<&str>>().len() == 1)
        {
            Ok(LinkType::Absolute(s.to_string()))
        } else {
            Err(MarkDownParserError::FailedToParseLink(s.to_string()))
        }
    }
}

#[derive(Debug)]
pub enum ImageType {
    NamedAbsolute(String, String),
    NamedRelative(String, String),
}

impl FromStr for ImageType {
    type Err = MarkDownParserError;
    fn from_str(s: &str) -> Result<ImageType, MarkDownParserError> {
        if s.starts_with("![") & s.ends_with(')') & s.find('\n').is_none() {
            if let Some(open_pos) = s.find('(') {
                // ("![TEXT]", "(URL)")
                let (unparsed_text, unparsed_url) = s.split_at(open_pos);
                // "TEXT"
                let parsed_text = unparsed_text
                    .trim_left_matches('!')
                    .trim_left_matches('[')
                    .trim_right_matches(']')
                    .to_string();
                // "URL"
                let parsed_url = unparsed_url
                    .trim_left_matches('(')
                    .trim_right_matches(')')
                    .trim()
                    .to_string();
                if parsed_url.split_whitespace().collect::<Vec<&str>>().len() != 1 {
                    Err(MarkDownParserError::FailedToParseImage(s.to_string()))
                } else if parsed_url.find("://").is_some() {
                    Ok(ImageType::NamedAbsolute(parsed_text, parsed_url))
                } else {
                    Ok(ImageType::NamedRelative(parsed_text, parsed_url))
                }
            } else {
                Err(MarkDownParserError::FailedToParseImage(s.to_string()))
            }
        } else {
            Err(MarkDownParserError::FailedToParseImage(s.to_string()))
        }
    }
}

#[derive(Debug)]
pub enum CodeBlockType {
    Inline(String),
    MultiLine(Option<String>, String),
}

impl CodeBlockType {
    /// Take a codeblock as a string, return CodeBlockType::MultiLine
    /// Will strip all backticks to create the Multiline Codeblock
    /// Option<String> will be the first word of the first line if it isnt "```\n"
    /// String will still contain the first word and the rest of the codeblock
    fn is_multiline(s: String) -> CodeBlockType {
        let txt = s.trim_left_matches("```").trim_right_matches("```");
        let option = txt.chars()
            .next()
            .and_then(|c| if c == '\n' { None } else { txt.lines().next() })
            .and_then(|line| line.split_whitespace().next())
            .and_then(|first| {
                if first.is_empty() {
                    None
                } else {
                    Some(first.to_string())
                }
            });
        CodeBlockType::MultiLine(option, txt.trim().to_string())
    }
}

// Box<FormattingType> should never be the exact enum arm
#[derive(Debug)]
pub enum FormattingType {
    /// __Underline__ Formatting
    Underline(Box<FormattingType>),
    /// **Bold** Formatting
    Bold(Box<FormattingType>),
    /// ~~StrikeThroguh~~ Formatting
    StrikeThrough(Box<FormattingType>),
    /// "Quotes" Formatting
    Quotes(Box<FormattingType>),
    CodeBlock(CodeBlockType),
    Plain(String),
    Link(LinkType),
}

#[derive(Debug)]
pub enum ListType {
    Numbered(usize, String),
    Bullet(String),
    Custom(String, String),
    UnOrd(String),
    Nested(usize, Box<ListType>),
}

impl FromStr for ListType {
    type Err = MarkDownParserError;
    fn from_str(s: &str) -> Result<ListType, MarkDownParserError> {
        if (s.trim().len() == 0) | s.find('\n').is_some() {
            Err(MarkDownParserError::FailedToParseList(s.to_string()))
        } else if s.starts_with("-{") {
            if let Some(close) = s.find('}') {
                let (custom, entry) = s.split_at(close);
                // panic!("{}", custom);
                if entry.starts_with("} ") {
                    let entry = entry.trim_left_matches("} ").to_string();
                    let custom = custom.trim_left_matches("-{").to_string();
                    Ok(ListType::Custom(custom, entry))
                } else {
                    Err(MarkDownParserError::FailedToParseList(s.to_string()))
                }
            } else {
                Err(MarkDownParserError::FailedToParseList(s.to_string()))
            }
        } else if s.starts_with("- ") {
            Ok(ListType::UnOrd(s.trim_left_matches("- ").to_string()))
        } else if s.starts_with("* ") {
            Ok(ListType::Bullet(s.trim_left_matches("* ").to_string()))
        } else if s.starts_with("    ") {
            let (indent, list_entry) = s.split_at(s.len() - s.trim().len());
            Ok(ListType::Nested(
                indent.len(),
                Box::new(list_entry.parse()?),
            ))
        } else {
            if let Some(num_index) = s.find('.') {
                let (num, txt) = s.split_at(num_index);
                if num.chars().all(|c| c.is_digit(10)) {
                    if let Ok(num) = num.parse() {
                        Ok(ListType::Numbered(
                            num,
                            txt.trim_left_matches(". ").to_string(),
                        ))
                    } else {
                        Err(MarkDownParserError::FailedToParseList(s.to_string()))
                    }
                } else {
                    Err(MarkDownParserError::FailedToParseList(s.to_string()))
                }
            } else {
                Err(MarkDownParserError::FailedToParseList(s.to_string()))
            }
        }
    }
}

#[derive(Debug)]
pub enum MarkDown {
    List(Vec<ListType>),
    Image(ImageType),
    Text(Vec<FormattingType>),
    Header(Headers),
}

#[derive(Debug)]
pub enum MarkDownParserError {
    FailedToParse(String),
    FailedToParseHeader(String),
    FailedToParseImage(String),
    FailedToParseLink(String),
    FailedToParseList(String),
}

/// New Text, AmountTrimmed (Start)
/// Only the Start is returned to match the Header
/// White Space isnt counted
fn trim_hashtags(txt: &str) -> (&str, usize) {
    let checkpoint = txt.clone();
    let txt = txt.trim_left_matches('#');
    if !txt.starts_with(' ') {
        return (checkpoint, 0);
    }
    let start = checkpoint.len() - txt.len();
    let txt = txt.trim_left();
    let checkpoint = txt.clone();
    let txt = txt.trim_right_matches('#');
    if !txt.ends_with(' ') {
        (checkpoint, start)
    } else {
        (txt.trim(), start)
    }
}

// TODO: Ignore escaped backticks
/// Removes the mulit line codeblocks from the string, they are immune to formatting
/// Returns based on index
/// 0: Vec<String> these are the codeblocks
/// 1: String this is the string with the codeblocks removed
/// 3: This is the range in which they were removed
fn get_multiline_code_blocks(s: &str) -> Option<(CodeBlockType, String)> {
    let mut in_codeblock = false;
    let mut tick_counter: usize = 0;
    let mut code_block: String = String::new();
    let mut filtered_pos: Vec<usize> = vec![];
    // Iterate through string finding the codeblock
    for (i, c) in s.char_indices() {
        // If backtick found and not in codeblock
        if c == '`' && !in_codeblock {
            // Increment
            tick_counter += 1;
            code_block.push(c);
            filtered_pos.push(i);
            if tick_counter == 3 {
                in_codeblock = true;
            }
        // If the char after the backtick while not in a codeblock  (when backtick isnt 3) cancel the codeblock
        } else if c != '`' && !in_codeblock && tick_counter != 0 {
            tick_counter = 0;
            code_block.clear();
            filtered_pos.clear();
        // If a backtick was found while in a codeblock but the char after it wasnt a backtick, cancel the countdown
        } else if c != '`' && in_codeblock && tick_counter != 3 {
            tick_counter = 3;
            code_block.push(c);
            filtered_pos.push(i);
        // Count Down the backtick counter to find end of codeblock
        } else if c == '`' && in_codeblock {
            tick_counter -= 1; // Count down the  codeblock closure
            code_block.push(c);
            filtered_pos.push(i);
            if tick_counter == 0 {
                // Break at end of codeblock
                break;
            }
        } else if in_codeblock {
            code_block.push(c);
            filtered_pos.push(i);
        }
    }
    if code_block.is_empty() || tick_counter != 0 {
        None
    } else {
        Some((
            CodeBlockType::is_multiline(code_block),
            s.char_indices().fold(String::new(), |mut s, (i, c)| {
                if !filtered_pos.contains(&i) {
                    s.push(c)
                }
                s
            }),
        ))
    }
}

fn process_text(s: &str) -> Vec<FormattingType> {
    vec![]
}

impl FromStr for MarkDown {
    type Err = MarkDownParserError;
    fn from_str(s: &str) -> Result<MarkDown, Self::Err> {
        if let Ok(header) = s.parse() {
            Ok(MarkDown::Header(header))
        } else if let Ok(img) = s.parse() {
            Ok(MarkDown::Image(img))
        } else if let Ok(link) = s.parse() {
            Ok(MarkDown::Text(vec![FormattingType::Link(link)]))
        } else if let Ok(list) = s.parse() {
            Ok(MarkDown::List(vec![list]))
        } else {
            Err(MarkDownParserError::FailedToParse(s.to_string()))
        }
    }
}

pub fn parse<T: ToString>(txt: T) -> Result<Vec<MarkDown>, MarkDownParserError> {
    let mut tmp_vec: Vec<ListType> = vec![];
    let mut alt_tmp_vec: Vec<FormattingType> = vec![];
    let mut is_vec = false;
    let mut is_txt = false;
    let mut result = vec![];
    for line in txt.to_string().lines() {
        if let Ok(md) = MarkDown::from_str(line) {
            match md {
                MarkDown::List(list) => {
                    if !is_vec {
                        is_vec = true;
                    }
                    if is_txt {
                        result.push(MarkDown::Text(alt_tmp_vec));
                        is_txt = false;
                        alt_tmp_vec = vec![];
                    }
                    list.into_iter().for_each(|e| tmp_vec.push(e));
                }
                MarkDown::Image(img) => {
                    if is_vec {
                        result.push(MarkDown::List(tmp_vec));
                        is_vec = false;
                        tmp_vec = vec![];
                    }
                    if is_txt {
                        result.push(MarkDown::Text(alt_tmp_vec));
                        is_txt = false;
                        alt_tmp_vec = vec![];
                    }
                    result.push(MarkDown::Image(img));
                }
                MarkDown::Text(text) => {
                    if is_vec {
                        result.push(MarkDown::List(tmp_vec));
                        is_vec = false;
                        tmp_vec = vec![];
                    }
                    if !is_txt {
                        is_txt = true;
                    }
                    for txt in text {
                        alt_tmp_vec.push(txt)
                    }
                }
                MarkDown::Header(header) => {
                    if is_vec {
                        result.push(MarkDown::List(tmp_vec));
                        is_vec = false;
                        tmp_vec = vec![];
                    }
                    if is_txt {
                        result.push(MarkDown::Text(alt_tmp_vec));
                        is_txt = false;
                        alt_tmp_vec = vec![];
                    }
                    result.push(MarkDown::Header(header));
                }
            }
        } else {
        }
    }
    if is_txt {
        result.push(MarkDown::Text(alt_tmp_vec));
    }
    if is_vec {
        result.push(MarkDown::List(tmp_vec))
    }
    Ok(result)
}

#[cfg(test)]
mod test {
    use super::{get_multiline_code_blocks, trim_hashtags as do_trim, CodeBlockType};
    #[test]
    fn trim_hashtags() {
        [
            "## Hello World",
            "#### Something Something",
            "### This is a test ###",
        ].iter()
            .for_each(|txt| {
                let (new_txt, _) = do_trim(txt);
                assert_eq!(new_txt.find('#'), None)
            })
    }
    #[test]
    fn filter_multiline_code_blocks() {
        {
            let text = "You can print hello world in python by typing\n```python\nprint(\"Hello World\")\n```\nin the interpriter";
            let (_, new_text) =
                get_multiline_code_blocks(text).expect("Expected to find codeblock, found nothing");
            assert_eq!(
                new_text,
                "You can print hello world in python by typing\n\nin the interpriter"
            );
        }
        {
            let text = "```python\nprint(\"Hello World\")``` I `love` cake";
            let (codeblock, new_text) = get_multiline_code_blocks(text)
                .expect("Expected  to find codeblock, found nothing");
            assert_eq!(new_text, " I `love` cake");
            match codeblock {
                CodeBlockType::MultiLine(lang, txt) => {
                    assert_eq!(lang, Some(String::from("python")));
                    assert_eq!(txt, "python\nprint(\"Hello World\")");
                }
                _ => unreachable!(),
            }
        }
    }

}
